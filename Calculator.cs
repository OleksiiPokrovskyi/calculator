namespace MyCalculator
{
    using System;
    internal class Calculator : ICalculator {
        public int Add (int a, int b) {
            return a + b;
        }
        public int Mul (double a, double b) {
            return (int)(a * b);
        }
    }
}