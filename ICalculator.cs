namespace MyCalculator
{
    using System;
    internal interface ICalculator {
        int Add (int a, int b);
        int Mul (double a, double b);
    }
}